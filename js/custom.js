/*	Responsive Boilerplate
		Designed & Built by Fernando Monteiro, http://www.newaeonweb.com.br
		Licensed under MIT license, http://opensource.org/licenses/mit-license.php
*/

//Hide the url bar on smartphones
/*
 * Normalized hide address bar for iOS & Android
 * (c) Scott Jehl, scottjehl.com
 * MIT License
 */
(function (win) {
	var doc = win.document;

	// If there's a hash, or addEventListener is undefined, stop here
	if (!location.hash && win.addEventListener) {

		//scroll to 1
		window.scrollTo(0, 1);
		var scrollTop = 1,
			getScrollTop = function () {
				return win.pageYOffset || doc.compatMode === "CSS1Compat" && doc.documentElement.scrollTop || doc.body.scrollTop || 0;
			},

		//reset to 0 on bodyready, if needed
			bodycheck = setInterval(function () {
				if (doc.body) {
					clearInterval(bodycheck);
					scrollTop = getScrollTop();
					win.scrollTo(0, scrollTop === 1 ? 0 : 1);
				}
			}, 15);

		win.addEventListener("load", function () {
			setTimeout(function () {
				//at load, if user hasn't scrolled more than 20 or so...
				if (getScrollTop() < 20) {
					//reset to hide addr bar at onload
					win.scrollTo(0, scrollTop === 1 ? 0 : 1);
				}
			}, 0);
		});
	}
})(this);


//Simple function to a responsive menu
(function () {
	//$("#nav").before('<div id="menu"><div class="middle"><span>&#9776</span></div></div>');

	$("#menu").on('click', function () {
		$("#nav").toggle();
	});

	$(window).resize(function () {
		if (window.innerWidth > 768) {
			$("#nav").removeAttr("style");
		}
	});
})(this);
//apply to any markup with nav like this:
/*

 <ul id="nav">
 <li><a href="#">Link</a></li>
 <li><a href="#">Link</a></li>
 <li><a href="#">Link</a></li>
 <li><a href="#">Link</a></li>
 <li><a href="#">Link</a></li>
 </ul>


 */

//Add your scripts here

//GALLERY
var gallery = document.getElementById('mySwipe');
window.mySwipe = Swipe(gallery, {
	// startSlide: 4,
	auto: 5000,
	//continuous: true,
	// disableScroll: true,
	// stopPropagation: true,
	// callback: function(index, element) {},
	// transitionEnd: function(index, element) {}
});

document.getElementById('galleryPrevBtn').onclick = function(){
	ga('send', 'event', 'Gallery', 'Prev Click');
	mySwipe.prev();
};
document.getElementById('galleryNextBtn').onclick = function(){
	ga('send', 'event', 'Gallery', 'Next Click');
	mySwipe.next();
};

//MENU
$("#logoColor").click(function(){
	$("#nav").removeAttr("style");

	$.scrollTo(0,0, {
		duration: 500,
		onAfter: function(){
			ga('send', 'event', 'Menu', 'Logo Click');
		}
	});
});

$("#nav a").click(function(){
	var section		= $(this).data("navitem");
	var scrollElm	= $("#" + section);

	$("#nav").removeAttr("style");

	var windowWidth = $(window).width();
	var navOffset = -50; //for bigger than 1024
	
	//window less than 1025
	if (windowWidth <= 1024) {
		//between 481 and 768
		if (windowWidth > 480 && windowWidth <= 768) {
			navOffset = -50;
		}
		//between 769 and 1024 or less than 481
		else {
			navOffset = -30;
		}
	}

	$.scrollTo(scrollElm, {
		offset: navOffset, //little less than height of the menu
		duration: 500,
		onAfter: function(){
			ga('send', 'event', 'Menu', section + ' Click');
		}
	});
});
//flags
$("#flagPL").click(function(){
	ga('send', 'event', 'Menu', 'FlagPL Click');
	window.location = "/";
});

$("#flagUK").click(function(){
	ga('send', 'event', 'Menu', 'FlagEN Click');
	window.location = "/en";
});

//CONTACT FORM
//there is some JS validation for this form in the includes/email.php file


//FOOTER
document.getElementById('facebookBtn').onclick = function(){
	ga('send', 'event', 'Footer', 'Facebook Click');
	window.open("http://www.facebook.com/pages/BK-Logistyka/309065945929601", "_blank");
};
document.getElementById('twitterBtn').onclick = function(){
	ga('send', 'event', 'Footer', 'Twitter Click');
	window.open("http://twitter.com/BK_Logistyka", "_blank");
};
















