<?php
//DEBUG
ini_set("display_errors", "0"); //1 for debug
error_reporting(0);//E_ALL for debug
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>BK Logistyka - Rzetelność i Precyzja - Transport i Spedycja w Krakowie</title>
		<meta name="description" content="BK Logistyka to prężnie rozwijająca się firma logistyczna opierająca się głównie na międzynarodowym transporcie drogowym.">
		<meta name="author" content="Kamil Paluch">
		<meta name="keywords" content="BK Logistyka, Logistyka, Transport, Logistyka Kraków, Logistics, Firma Transportowa, Spedycja">
		<!-- Optimized mobile viewport -->
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<link rel="shortcut icon" href="/images/favicon.ico">
		<!--
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
		-->
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" media="all" href="/css/responsiveboilerplate.min.css">
		<!-- HTML5 IE Enabling Script -->
		<!--[if lt IE 9]>
		<script src="libs/html5shiv.min.js"></script>
		<![endif]-->

		<script type="text/javascript">
		//CONTACT FORM validation
		function validateContactForm() {
			//reset all
			var formValid = true;
			$("#contactFormMessages").html("");
			$("#inputName, #inputEmail, #formMessage").css("border-color", "#dddddd");

			if($("#inputName").val() == "") {
				//alert('n empty');
				$("#inputName").css('border-color', "#ed2124");
				$("#contactFormMessages").append("<?php echo '<p id=\"messageBoxResult\" class=\"fail\">'.$error_name_blank.'</p>'; ?>");
				formValid = false;
			}

			if($("#inputEmail").val() == "") {
				//alert('email empty');
				$("#inputEmail").css('border-color', "#ed2124");
				$("#contactFormMessages").append("<?php echo '<p id=\"messageBoxResult\" class=\"fail\">'.$error_email_blank.'</p>'; ?>");
				formValid = false;
			}

			if($("#formMessage").val() == "") {
				//alert('message empty');
				$("#formMessage").css('border-color', "#ed2124");
				$("#contactFormMessages").append("<?php echo '<p id=\"messageBoxResult\" class=\"fail\">'.$error_message_blank.'</p>'; ?>");
				formValid = false;
			}

			return formValid;
		}
		</script>
	</head>
<body>