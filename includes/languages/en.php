<?php

//META
$meta_title			= 'BK Logistyka - Reliability and Precision - Transport and Spedition in Kraków';


//MENU
$menu_services		= 'Services';
$menu_contact 		= 'Contact';
$menu_about 		= 'About';
$menu_careers		= 'Careers';

//SERVICES
$services_headline		= 'We value good relationships with our contractors';
$services_tagline		= 'We use the most up-to-date transportation solutions to conduct our business. We have access to large fleet of smaller trucks, busses and vans, that allow us to maximize our efficiency and shorten your wait time';
$services_trucks 		= 'We specialize in international shipping and transportation using trucks with 13.6m trailers (taut liners, tilt trailers), road trains (7.7m + 7.7m) and busses';
$services_destinations	= 'Our main destinations are Germany, Netherlands, Belgium, France, Switzerland, Chech Republic, Italy, and Slovakia, but by leveraging the wide reach of our hauliers we can arrange transportation to any place in Europe';
$services_materials		= 'We transport such products as: wood, aluminum, alcohol, food, glass, stone, steel, flammable materials (ADR) and much more';

//ABOUT
$about_headline			= 'Positive attitude is our way to success';
$about_tagline			= 'Our main goal is to build and maintain long-term relationship with our client by researching and applying solutions that are the most appropriate at a given time';
$about_sub_headline		= 'Reliability is our Trademark';
$about_main_1			= 'BK Logistics is a company established on principles of precision and reliability. Based on our team’s international transport experience, we guarantee the highest quality of service in this business';
$about_main_2			= 'We established the company in 2013 but we have more than 10 years of combined experience in the logistics business. Dont let the size of our team fool you, there are only trhree of us but we work harder than ten-person squads';
$about_main_3			= 'Rates are calculated quickly, in nice and professional environment. If you are a haulier or a producer looking for reliable shipping company, look no further! Ask as a question through e-mail, contact form below or call to us to save your precious time';
$about_team_headline	= 'Our Work Team';
$mirek_position			= 'Transportation & Logistics';
$klaudia_position		= 'Transportation & Logistics';
$kinga_position			= 'Book-keeping';

//EMAIL
$email_subject			= 'Message from BK Logistyka website';

//CONTACT FORM
$contact_line1			= 'You can contact us by sending an email to:';
$contact_line3			= 'or by using the form below';
$form_info_name			= 'Name';
$form_info_email		= 'E-Mail';
$form_info_message		= 'Message';
$form_btn_send			= 'Send';
$form_result_fail 		= 'Message not sent! It appears that something went wrong :( <br />Please send email to: info@bklogistyka.com or call us!';
$form_result_success	= 'Thanks for your message! <br />We will get back to you as soon as possible!';
$form_result_warning	= 'Form fields can not be empty';

$error_name_blank		= 'Please fill out name field';
$error_email_blank		= 'Please provide your email address';
$error_message_blank	= 'Please make sure that the message field is not empty';

//ADDRESS
$address_line1			= 'Ul. Poznańska 6/19. 30-012 Kraków, Poland';
$address_tel			= 'Telephone:';
$address_tel_abv		= 'Tel:';
$address_cell			= 'Cell Phone:';
$address_cell_abv		= 'Cell:';
$driving_directions		= 'Click here for driving directions';

//FOOTER
$foot_copyright			= '2014 BK Logistyka. All Rights Reserved';
$foot_design			= 'Design:';
$foot_coding			= 'Coding:';
?>