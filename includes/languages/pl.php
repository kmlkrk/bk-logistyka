<?php

//META
$meta_title			= 'BK Logistyka - Rzetelność i Precyzja - Transport i Spedycja w Krakowie';


//MENU
$menu_services		= 'Usługi';
$menu_contact 		= 'Kontakt';
$menu_about 		= 'O Nas';
$menu_careers		= 'Kariera';

//SERVICES
$services_headline		= 'Cenimy współpracę opartą na dobrych relacjach z kontrahentami';
$services_tagline		= 'W naszych działaniach korzystamy z nowoczesnych rozwiązań logistycznych. Dysponujemy dużą flotą mniejszych samochodów ciężarowych oraz busów aby w razie potrzeby skrócić czas oczekiwania na Państwa ładunek (w wypadku ładunków małych LTL)';
$services_trucks 		= 'Specjalizujemy się głównie w spedycji i transporcie międzynarodowym przy pomocy pojazdów z naczepami 13,6m. typu firanka, burtówka, zestawów (7,7m + 7,7m) oraz busów';
$services_destinations	= 'Korzystając z bardzo szeroko rozbudowanej sieci przewoźników jesteśmy w stanie zrealizować każdy transport na terenie Europy. Nasze preferowane kierunki to Niemcy, Holandia, Belgia, Francja, Szwajcaria, Czechy, Słowacja i Włochy';
$services_materials		= 'Przewozimy takie materiały jak drewno, aluminium, alkohol, materiały spożywcze, materiały budowlane, szkło, kamienie, stal, materiały łatwopalne ADR oraz wiele innych';

//ABOUT
$about_headline			= 'Pozytywne nastawienie jest nasza drogą do sukcesu';
$about_tagline			= 'Naszym głównym założeniem jest budowanie długotrwałych, obustronnie rzetelnych relacji z klientem, wyszukując precyzyjnie rozwiązania, które są dla niego w danym momencie odpowiednie';
$about_sub_headline		= 'Cechuje nas Rzetelność';
$about_main_1			= 'BK Logistyka jest firmą, która powstała w myśl domen precyzji oraz rzetelności. Opierając się na doświadczeniu naszego zespołu w zakresie transportu międzynarodowego, jesteśmy w stanie zagwarantować najwyższej jakości usługi w tej dziedzinie';
$about_main_2			= 'Założyliśmy naszą firmę w roku 2013 ale łącznie mamy ponad 10 lat doświadczenia w transporcie i logistyce. Nasz zespół jest póki co mały ale niech Cię to nie zmyli gdyż pracujemy cieżej niż niejedna dziesięcio osobowa drużyna!';
$about_main_3			= 'Stawki obliczane są na bieżąco, szybko, w miłej i profesjonalnej atmosferze. Jesli jesteś przewoźnikiem lub producentem poszukującym rzetelnej spedycji zadaj nam pytanie poprzez e-mail, forme kontaktową poniżej, lub zadzwoń aby zaoszczędzić cenny czas!';
$about_team_headline	= 'Nasza Załoga';
$mirek_position			= 'Transport i Logistyka';
$klaudia_position		= 'Transport i Logistyka';
$kinga_position			= 'Księgowość';

//EMAIL
$email_subject			= 'Wiadomosc ze strony BK Logistyka';

//CONTACT FORM
$contact_line1			= 'Możesz się z nami skontaktować poprzez email:';
$contact_line3			= 'lub skorzystaj z poniższej formy';
$form_info_name			= 'Imię';
$form_info_email		= 'E-Mail';
$form_info_message		= 'Wiadomość';
$form_btn_send			= 'Wyślij';
$form_result_fail 		= 'Wiadomość nie wysłana! Coś poszło nie tak :( <br />Proszę wysłać email do: info@bklogistyka.com lub do nas zadzwonić!';
$form_result_success	= 'Dziękujemy za wiadomość! <br />Skontaktujemy się z Tobą tak szybko jak to możliwe!';
$form_result_warning	= 'Żadne pole nie może być puste';

$error_name_blank		= 'Proszę wypełnić pole z imieniem';
$error_email_blank		= 'Proszę podać swój adres email';
$error_message_blank	= 'Proszę się upewnić że pole z wiadomością nie jest puste';

//ADDRESS
$address_line1			= 'Ul. Poznańska 6/19. 30-012 Kraków';
$address_tel			= 'Telefon:';
$address_tel_abv		= 'Tel:';
$address_cell			= 'Komórka:';
$address_cell_abv		= 'Kom:';
$driving_directions		= 'Kliknij tutaj aby uzyskać instrukcje dojazdu';

//FOOTER
$foot_copyright			= '2014 BK Logistyka. Wszystkie Prawa Zastrzeżone';
$foot_design			= 'Projekt:';
$foot_coding			= 'Kod:';
?>