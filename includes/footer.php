
<!-- JavaScript at the bottom for fast page loading -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/libs/jquery-1.10.1.min.js"><\/script>')</script>
<script src="/libs/swipe.min.js"></script>
<script src="/libs/jquery.scrollTo.min.js"></script>
<script src="/js/js/custom.min.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to your site's ID.-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52886808-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
//IE outdate friendly warning if you don't want remove it
var $buoop = {vs: {i: 8, f: 3.6, o: 10.6, s: 3.2, n: 9}}
$buoop.ol = window.onload;
window.onload = function () {
	try {
		if ($buoop.ol) $buoop.ol();
	} 
	catch (e) {
	}
	var e = document.createElement("script");
	e.setAttribute("type", "text/javascript");
	e.setAttribute("src", "http://browser-update.org/update.js");
	document.body.appendChild(e);
}
</script>
</body>
</html>