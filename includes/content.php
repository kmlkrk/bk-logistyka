<div id="wrapper" class="container">
	<!--There`s, place your columns right here!-->
	<!--Don't forget to use the class "content" outside the columns -->

	<div id="header" class="content">
		<div class="wrap">
			<div id="logoWrapper" class="col4">
				<img id="logo" src="/images/bk-logo.png" alt="Logo" />
				<img id="logoColor" src="/images/bk-logo-color.png" alt="" />

				<div id="menuTriggerHolder" class="middle"><img alt="" id="menu" src="/images/menu.png" /></div>

			</div>
			<div id="menuHolder" class="col8">
				<div class="middle">
					<ul id="nav">
						<!-- <li><a data-navitem="home" 		id="menu-home" 		href="#">Home</a></li> -->
						<li><span><img alt="" id="flagPL" src="/images/flag-pl-color.png" /><img alt="" id="flagUK" src="/images/flag-uk-color.png" /></span></li>

						<li><a data-navitem="services"  id="menu-services" 	href="javascript:void(0);"><?php echo $menu_services ?></a></li>
						<li><a data-navitem="about"  	id="menu-about" 	href="javascript:void(0);"><?php echo $menu_about ?></a></li>
						<li><a data-navitem="contact"  	id="menu-contact" 	href="javascript:void(0);"><?php echo $menu_contact ?></a></li>
						<!-- <li><a data-navitem="careers"  	id="menu-careers" 	href="javascript:void(0);"><?php echo $menu_careers ?></a></li> -->
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- GALLERY -->
	<div id="gallery" class="content">
		<div class="col12">
			<div id='mySwipe' class='swipe'>
				<div class='swipe-wrap'>
					<div><img alt="" src="/images/gallery/truck-1.jpg" /></div>
					<div><img alt="" src="/images/gallery/truck-2.jpg" /></div>
					<div><img alt="" src="/images/gallery/truck-3.jpg" /></div>
					<div><img alt="" src="/images/gallery/truck-4.jpg" /></div>
				</div>
				<img alt="" id="galleryPrevBtn" src="/images/btn-left.png" /> 
  				<img alt="" id="galleryNextBtn" src="/images/btn-right.png" />
			</div>
			
		</div>
	</div>


	<div id="services" class="content">
		<div class="wrap">
			<div class="col12"><h2><?php echo $menu_services ?></h2></div>

			<div class="content">
				<div class="headlineHolder col12">
					<h4><?php echo $services_headline ?></h4>
					<p><?php echo $services_tagline ?></p>
				</div>
			</div>

			<div id="servicesInfoHolder" class="content">
				<div id="servicesTrucks" class="col4">
					<img alt="" class="servicesIcon" src="/images/icon-truck-2.png" />
					<p><?php echo $services_trucks ?></p>
				</div>
				
				<div id="servicesDestinations" class="col4">
					<img alt="" class="servicesIcon" src="/images/icon-globe-2.png" />
					<p><?php echo $services_destinations ?></p>
				</div>
				
				<div id="servicesMaterials" class="col4">
					<img alt="" class="servicesIcon" src="/images/icon-materials-2.png" />
					<p><?php echo $services_materials ?></p>
				</div>
			</div>

		</div>
	</div>

	<div id="about" class="content">
		<div class="wrap">
			<div class="col12"><h2><?php echo $menu_about ?></h2></div>

			<div class="content">
				<div class="headlineHolder col12">
					<h4><?php echo $about_headline ?></h4>
					<p><?php echo $about_tagline ?></p>
				</div>
			</div>

			<div id="aboutInfoHolder" class="content">
				<div id="aboutInfo" class="col6">
					<h4><?php echo $about_sub_headline ?></h4>
					<p><?php echo $about_main_1 ?></p>
					<p><?php echo $about_main_2 ?></p>
					<p><?php echo $about_main_3 ?></p>
				</div>
				
				<div id="aboutStaffHolder" class="col6">
					<img alt="" src="/images/staff-together.jpg" />
				</div>
			</div>

			<h3><?php echo $about_team_headline ?></h3>

			<div id="teamImagesHolder" class="content">
				<div class="col4 teamMember teamMember1">
					<img alt="" src="/images/staff-mirek.jpg" />
					<h6>Mirosław / <?php echo $mirek_position ?></h6>
				</div>
				
				<div class="col4 teamMember teamMember2">
					<img alt="" src="/images/staff-klaudia.jpg" />
					<h6>Klaudia / <?php echo $klaudia_position ?></h6>
				</div>

				<div class="col4 teamMember teamMember3">
					<img alt="" src="/images/staff-kinga.jpg" />
					<h6>Kinga / <?php echo $kinga_position ?></h6>
				</div>
			</div>

		</div>
	</div>


	<div id="contact" class="content">
		<div class="col12"><h2><?php echo $menu_contact ?></h2></div>

		<div class="content wrap">
			<div id="formHolder" class="col6">
				<p><?php echo $contact_line1 ?></p>
				<p><a href="mailto:info@bklogistyka.pl" target="_blank">info@bklogistyka.pl</a></p>
				<p><?php echo $contact_line3 ?></p>

				<?php
				 
				?>

				<!-- there is some JS validation for this form in the includes/email.php file-->
				<form id="contactForm" method="post" action="" onsubmit="return validateContactForm();">
        
					<label><?php echo $form_info_name ?></label>
					<input id="inputName" name="name" placeholder="" value="<?php if (isset($_POST['name'])) {echo $_POST['name']; } ?>">
					        
					<label><?php echo $form_info_email ?></label>
					<input id="inputEmail" name="email" type="email" placeholder="" value="<?php if (isset($_POST['email'])) {echo $_POST['email']; } ?>">
					        
					<label><?php echo $form_info_message ?></label>
					<textarea id="formMessage" name="message" placeholder=""><?php if (isset($_POST['message'])) {echo $_POST['message']; } ?></textarea>
					        
					<input id="submit" name="submit" type="submit" value="<?php echo $form_btn_send ?>">
				    
				</form>

				<div id="contactFormMessages">

				<?php
				//send the message, check for errors 
				if (isset($_POST['submit']) && ($_POST['submit']) == $form_btn_send) {

					?>
					<script type="text/javascript">
						//scroll to the section of the page with contact form so that user can see message
						document.addEventListener('DOMContentLoaded', function(){
							$.scrollTo($("#formMessage"), { 
								offset: -100,
								duration: 750,
								onAfter: function(){
									ga('send', 'event', 'Email', 'Send Message Click');
								}
							});
						});
					</script>
					<?

					if(!$mail->Send()) {
						echo '<p id="messageBoxResult" class="fail">'.$form_result_fail.'</p>';
						//echo 'Mailer error: ' . $mail->ErrorInfo;
					} else {
						echo '<p id="messageBoxResult" class="success">'.$form_result_success.'</p>';
					}
				}
				?>
				</div>
			</div>
			
			<div id="mapHolder" class="col6">
				<p><?php echo $address_line1 ?></p>
				<p><?php echo $address_tel ?> (012) 353-94-33</p>
				<p><?php echo $address_cell ?> 660-306-876</p>

				<div id="googleMap">
					<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div id="gmap_canvas_holder" style="overflow:hidden;height:500px;width:100%;"><div id="gmap_canvas" style="height:500px;width:100%;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}#maps{width:100%;font-size:10px;font-family:arial;text-align:right;}</style></div><script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script><script type="text/javascript">jQuery(document).ready(function(){jQuery('.gmap').hide();jQuery("#maps span").click(function() {var $this = $(this);$this.next("div").fadeToggle();$('.gmap').not($this.next("div")).fadeOut();});});</script><script type="text/javascript"> function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(50.0789786,19.925298200000043),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(50.0789786, 19.925298200000043)});infowindow = new google.maps.InfoWindow({content:"<b>BK Logistyka</b><br/>Ulica Poznanska 6/19<br/>30-012 Krak&oacute;w" });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
					<!-- <div id="maps"><span>Google Maps © 2014</span><div class="gmap">supported by <a href="http://www.sparbalu.com">sparbalu.com</a></div></div> -->

					<p id="driveDirections"><a href="https://www.google.com/maps/dir//Pozna%C5%84ska+6,+30-012+Krak%C3%B3w,+Poland/@50.0789786,19.9252982,17z/data=!4m13!1m4!3m3!1s0x47165bacbbebb75b:0x9d0aab7dba732fac!2sPozna%C5%84ska+6!3b1!4m7!1m0!1m5!1m1!1s0x47165bacbbebb75b:0x9d0aab7dba732fac!2m2!1d19.9252982!2d50.0789786" target="_blank"><?php echo $driving_directions ?></a></p>
				</div>
			</div>
		</div>
	</div>

	<div id="footer" class="content">
		<div class="wrap">
			<div id="address" class="col5">
				<div class="middle">
					<p><?php echo $address_line1 ?></p>
					<p><?php echo $address_tel_abv ?> (012) 353-94-33. <?php echo $address_cell_abv ?> 660-306-876</p>
					<p><?php echo $form_info_email ?> <a href="mailto:info@bklogistyka.pl" target="_blank">info@bklogistyka.pl</a></p>
				</div>
			</div>
			<div id="social" class="col2 center">
				<div class="middle">
					<img alt="" id="facebookBtn" src="/images/facebook-icon.png" />
					<img alt="" id="twitterBtn" src="/images/twitter-icon.png" />
				</div>
			</div>
			<div id="rights" class="col5 center">
				<div class="middle">
					<p><?php echo $foot_copyright ?></p>
					<p><?php echo $foot_design ?> Kamil &amp; Magdalena Paluch</p>
					<p><?php echo $foot_coding ?> Kamil Paluch / <a href="mailto:kamelnyc@gmail.com">kamelNYC@gmail.com</a></p>
				</div>
			</div>
		</div>
	</div>

</div>