TO DO:


- AJAXify contact form

- cookie to store selected language, on each load check cookie, if set for EN, then redirect to /EN
- basic SEO and Meta info






DONE:
- enable minified CSS
- enable minified JS
- enable GOOGLE analytics
- tablet layout
- phone layout
- add GA events on menu items,gallery, contact form etc
- set correct include path if it is not working (it should be but need to check)
- DISABLE php error reporting in both index files
- pre validate contact form with JS